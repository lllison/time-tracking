package com.example.timetracking.model;

import com.example.timetracking.util.TimeType;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.time.OffsetDateTime;
import java.util.UUID;

@Entity
@Table(name = "user_times")
@Getter
@Setter
@NoArgsConstructor
public class UserTime {
    @Id
    private UUID id;

    private UUID userId;
    private OffsetDateTime dateTime;
    private TimeType type;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "userId", insertable = false, updatable = false)
    private User user;
}
