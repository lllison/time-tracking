package com.example.timetracking.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.UUID;

@Entity
@Table(name = "users")
@Getter
@Setter
@NoArgsConstructor
public class User {
    @Id
    private UUID id = UUID.randomUUID();

    @Column(name = "name")
    private String username;

    @Column(name = "password")
    private String password;
}
