package com.example.timetracking.repository;

import com.example.timetracking.model.UserTime;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.UUID;

@Repository
public interface UserTimeRepository extends JpaRepository<UserTime, UUID> {
}
