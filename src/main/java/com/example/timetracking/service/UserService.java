package com.example.timetracking.service;

import com.example.timetracking.model.User;

public interface UserService {
    void save(User user);

    User findByUsername(String username);
}
